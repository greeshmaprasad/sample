package com.example.a14906.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
private TextView textViewen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText en_tv=findViewById(R.id.tv_encrypt);
        Button bt_en=findViewById(R.id.encrypt_bt);
        textViewen=findViewById(R.id.textView_en);
        bt_en.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = en_tv.getText().toString();
                encrypt(s);
            }
        });

    }
    private void  encrypt(String s){
        StringBuilder builder = new StringBuilder();
        char b,a;
        String demo="";
        char d;
        for (int i=0;i<s.length();i++) {

                 a = s.charAt(i);
                if (i!=s.length()-1){
                    b = s.charAt(i + 1);

                }else {
                    b=0;
                }
                int count = 1;
                if (a == b) {
                    count++;
                    String s1 = Integer.toString(count);
                    demo= ""+a+s1;
                    builder.append(demo);
                    i++;
                } else {
                   char t= (char) (a+count);
                    String s1 = Integer.toString(count);
                    demo=""+a+s1;

                    builder.append(demo);
                }
                count=1;
            }
            Log.e("decrypt", "onClick: " + builder);
        textViewen.setText(builder.toString());



    }
}
