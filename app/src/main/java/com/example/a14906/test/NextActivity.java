package com.example.a14906.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class NextActivity extends AppCompatActivity {
private TextView textViewde;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);
        final EditText de_tv=findViewById(R.id.tv_decrypt);
        Button bt_de=findViewById(R.id.decrypt_bt);
        textViewde=findViewById(R.id.textView_de);
        bt_de.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = de_tv.getText().toString();
                encrypt(s);

            }
        });
    }
    private void  encrypt(String encrypt){
        char sample = 0;
        int n = 0;
        String data="";
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < encrypt.length(); i++) {

            char c = encrypt.charAt(i);

            if(Character.isLetter(c) || c==' ' ){


                sample=c;

            }
            if (Character.isDigit(c))
            {

                n=Character.getNumericValue(c);
            }
            for (int j=0;j<n;j++){

                builder.append(sample);
            }
            n=0;


        }
        System.out.print("to string"+builder.toString());
        Log.e("encryption   ", "encrypt: "+builder.toString() );
        textViewde.setText(builder.toString());
    }
}
